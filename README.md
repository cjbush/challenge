**Problem Statement:**

Using the data files provided here (data.json & data.csv) create a program that will do the following:
Ingests both files Removes duplicate entries from the list

**Once the program has completed this task use the data to provide the following statistics:** 

* Total number of devices

* Total unique devices 

* Number of devices that fall within the follow geographic area: Longitude: -179.99 Latitude: 89.99, Longitude: 0.00, Latitude: 0.00, and their Device Ids

* Device ID with oldest update time, and what date/time that update was made 

* Device ID with newest update time, and what date/time that update was made

**Output the statistics in JSON format in a file called results.json**

Feel free to use any object oriented language. Good OO principles & code matters!

Good Luck,

*The Southern Cross Engineering Team*


**Data File Formats**

File 1 (CSV)

uuid (String) lat(double) long(double) timestamp(long)

File 2 (JSON)

uuid (String) lat(double) long(double) timestamp(long)