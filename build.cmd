@echo off
setlocal

set config=%1
if not defined config set config=Release

echo Ensure this command is run from Developer Command Prompt for VS 2017

call paket restore

msbuild SouthernCrossEngineering.Challenge.Bush.sln /p:Configuration=%config% /p:Platform="Any CPU" /t:Build /maxcpucount:%NUMBER_OF_PROCESSORS% /nr:false

vstest.console src\SouthernCrossEngineering.Challenge.Bush.Test\bin\%config%\SouthernCrossEngineering.Challenge.Bush.Test.dll /UseVsixExtensions:true /Enablecodecoverage /Platform:x64 /Logger:trx /inIsolation