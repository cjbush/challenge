﻿using CommandLine;
using CommandLine.Text;

namespace DeviceAnalyzer {
	internal sealed class CommandLineOptions {

		[Option('j', "json", DefaultValue = "data.json", HelpText = "The path to the JSON input file", Required = true)]
		public string JsonFile { get; set; }

		[Option('c', "csv", DefaultValue = "data.csv", HelpText = "The path to the CSV input file", Required = true)]
		public string CsvFile { get; set; }

		[Option('o', "output", DefaultValue = "result.json", HelpText = "The path to the output file", Required = true)]
		public string OutputFile { get; set; }

		[HelpOption]
		public string GetUsage() {
			return HelpText.AutoBuild(this, (current) => HelpText.DefaultParsingErrorsHandler(this, current));
		}

	}
}
