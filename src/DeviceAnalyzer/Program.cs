﻿using Common.Logging;
using Ninject;
using Nito.AsyncEx.Synchronous;
using SouthernCrossEngineering.Challenge.Bush;
using SouthernCrossEngineering.Challenge.Bush.IoCConfiguration;
using System;

namespace DeviceAnalyzer {
	internal static class Program {

		private static readonly ILog log = LogManager.GetLogger(nameof(Program));

		public static void Main(string[] args) {

			try {
				CommandLineOptions options = new CommandLineOptions();
				if (CommandLine.Parser.Default.ParseArguments(args, options)) {
					var ninject = new StandardKernel(new DeviceAnalysisModule());
					var analyzer = ninject.Get<IDeviceAnalyzer>();
					analyzer.Analyze(options.JsonFile, options.CsvFile, options.OutputFile).WaitAndUnwrapException();
				}
			} catch (Exception ex) {
				log.Error($@"An unhandled exception occurred.", ex); //Just to avoid a totally uhandled error
			}

		}
	}
}
