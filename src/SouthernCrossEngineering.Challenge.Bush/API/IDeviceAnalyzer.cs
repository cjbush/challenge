﻿using System.Threading.Tasks;

namespace SouthernCrossEngineering.Challenge.Bush {
	/// <summary>
	/// Main entry point for the analysis application. Has two methods for performing analysis on the specified input files
	/// </summary>
	public interface IDeviceAnalyzer {

		/// <summary>
		/// Analyze the two input files and return an analysis result object
		/// </summary>
		/// <param name="jsonFile">The path of the JSON file to parse</param>
		/// <param name="csvFile">The path of the CSV file to parse</param>
		/// <returns>A result of the analysis performed on the input</returns>
		Task<DeviceAnalysisResult> Analyze(string jsonFile, string csvFile);

		/// <summary>
		/// Analyze the two input files and write the resulting output to the specified JSON file
		/// </summary>
		/// <param name="jsonFile">The path of the JSON file to parse</param>
		/// <param name="csvFile">The path of the CSV file to parse</param>
		/// <param name="outputFile">The path of the output JSON file</param>
		Task Analyze(string jsonFile, string csvFile, string outputFile);

	}
}
