﻿using System;
using System.Collections.Generic;

namespace SouthernCrossEngineering.Challenge.Bush {
	/// <summary>
	/// Device model object
	/// </summary>
	public sealed class Device {
		/// <summary>
		/// The Unique ID of the device
		/// </summary>
		public Guid Uuid { get; set; }

		/// <summary>
		/// The latitude of the device
		/// </summary>
		public double Lat { get; set; }

		/// <summary>
		/// The longitude of the device
		/// </summary>
		public double Long { get; set; }

		/// <summary>
		/// The timestamp of the device's last update
		/// </summary>
		public long Timestamp { get; set; }

	}

	/// <summary>
	/// Collection of devices (used for easily deserializing the JSON input)
	/// </summary>
	internal sealed class DeviceList {
		/// <summary>
		/// Array of device objects
		/// </summary>
		public IEnumerable<Device> Devices { get; set; }
	}
}
