﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SouthernCrossEngineering.Challenge.Bush {
	/// <summary>
	/// Analysis result model object
	/// </summary>
	public sealed class DeviceAnalysisResult {

		/// <summary>
		/// Total number of devices found
		/// </summary>
		public int TotalNumberOfDevices { get; set; }

		/// <summary>
		/// Total number of unique devices
		/// </summary>
		public int TotalNumberOfUniqueDevices { get; set; }

		/// <summary>
		/// IDs of devices in the range lat(0.0, 89.99) lng(0.0, -179.99)
		/// </summary>
		public IEnumerable<Guid> DevicesInRange { get; set; }

		/// <summary>
		/// Number of devices in the range lat(0.0, 89.99) lng(0.0, -179.99)
		/// </summary>
		public int NumberOfDevicesInRange {
			get {
				return DevicesInRange?.Count() ?? 0;
			}
		}

		/// <summary>
		/// Device IDs with least recent update timestamps
		/// </summary>
		public IEnumerable<Guid> OldestUpdatedIds { get; set; }

		/// <summary>
		/// Timestamp of least recent device update
		/// </summary>
		public long OldestUpdatedTimestamp { get; set; }

		/// <summary>
		/// Device IDs with most recent update timestamps
		/// </summary>
		public IEnumerable<Guid> NewestUpdatedIds { get; set; }

		/// <summary>
		/// Timestamp of most recent device update
		/// </summary>
		public long NewestUpdatedTimestamp { get; set; }
	}
}
