﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SouthernCrossEngineering.Challenge.Bush {
	internal interface IDeviceParser {
		Task<IEnumerable<Device>> Parse(string file);
	}
}
