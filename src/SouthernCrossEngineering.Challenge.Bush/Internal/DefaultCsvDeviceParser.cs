﻿using Common.Logging;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SystemInterface.IO;

namespace SouthernCrossEngineering.Challenge.Bush {
	internal sealed class DefaultCsvDeviceParser : ICsvDeviceParser {

		private readonly ILog log;
		private readonly IFile File;

		// I know that injecting a logger is slightly unorthodox, but it allows you to mock it and verify that you actually log a warning when something bad happens
		// IFile is part of an open source library I contribute to on GitHub. It wraps system types such as System.IO.File so that you can mock it in unit testing
		public DefaultCsvDeviceParser(ILog log, IFile File) {
			this.log = log;
			this.File = File;
		}

		public Task<IEnumerable<Device>> Parse(string file) {
			var devices = new List<Device>();
			try {
				using (var fs = File.OpenRead(file))
				using (var parser = new TextFieldParser(fs.StreamInstance)) {
					parser.TextFieldType = FieldType.Delimited;
					parser.SetDelimiters(",");
					bool headersSkipped = false;
					long lastLine = 0;
					while (!parser.EndOfData) {
						if (parser.LineNumber != -1) {
							lastLine = parser.LineNumber;
						}
						var fields = parser.ReadFields();
						if (!headersSkipped) {
							headersSkipped = true;
							continue; // Skip the header line
						}
						try {
							var uuid = Guid.Parse(fields[0]);
							var lat = double.Parse(fields[1]);
							var lng = double.Parse(fields[2]);
							var timestamp = long.Parse(fields[3]);
							devices.Add(new Device { Uuid = uuid, Lat = lat, Long = lng, Timestamp = timestamp });
						} catch (Exception ex) when (ex is IndexOutOfRangeException || ex is FormatException || ex is MalformedLineException) {
							log.Error($@"Line {lastLine} was incorrectly formatted. Skipping to the next line.");
						}
					}
				}
			} catch (FileNotFoundException) {
				log.Error($@"CSV file {file} was not found.");
			}
			return Task.FromResult((IEnumerable<Device>)devices);
		}
	}
}
