﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SouthernCrossEngineering.Challenge.Bush {

	internal static class DistinctLambdaExtension {
		public static IEnumerable<T> Distinct<T, TKey>(this IEnumerable<T> list, Func<T, TKey> func) {
			return list.GroupBy(func).Select(t => t.First());
		}
	}

	internal sealed class DefaultDeviceAnalyzer : IDeviceAnalyzer {

		private readonly IJsonDeviceParser jsonParser;
		private readonly ICsvDeviceParser csvParser;
		private readonly IDeviceAnalysisResultWriter resultWriter;
		private readonly ILog log;

		public DefaultDeviceAnalyzer(IJsonDeviceParser jsonParser, ICsvDeviceParser csvParser, IDeviceAnalysisResultWriter resultWriter, ILog log) {
			this.jsonParser = jsonParser;
			this.csvParser = csvParser;
			this.resultWriter = resultWriter;
			this.log = log;
		}

		public async Task<DeviceAnalysisResult> Analyze(string jsonFile, string csvFile) {
			var jsonDevices = await jsonParser.Parse(jsonFile);
			log.Debug($@"JSON parser found {jsonDevices?.Count() ?? 0} devices");
			var csvDevices = await csvParser.Parse(csvFile);
			log.Debug($@"CSV parser found {csvDevices?.Count() ?? 0} devices");
			var unique = jsonDevices.Union(csvDevices).Distinct(d => d.Uuid); //join the two lists and remove duplicate uuids with our extension method
			log.Debug($@"{unique?.Count() ?? 0} devices were found.");
			var newest = unique.Where(d => d.Timestamp == unique.Max(d2 => d2.Timestamp));
			log.Debug($@"Newest devices: {string.Join(",", newest?.Select(d => d.Uuid) ?? new Guid[] { })}");
			var oldest = unique.Where(d => d.Timestamp == unique.Min(d2 => d2.Timestamp));
			log.Debug($@"Oldest devices: {string.Join(",", oldest?.Select(d => d.Uuid) ?? new Guid[] { })}");
			return new DeviceAnalysisResult {
				TotalNumberOfDevices = jsonDevices.Count() + csvDevices.Count(),
				TotalNumberOfUniqueDevices = unique.Count(),
				NewestUpdatedIds = newest.Select(d => d.Uuid),
				OldestUpdatedIds = oldest.Select(d => d.Uuid),
				NewestUpdatedTimestamp = newest.FirstOrDefault()?.Timestamp ?? default(long),
				OldestUpdatedTimestamp = oldest.FirstOrDefault()?.Timestamp ?? default(long),
				DevicesInRange = unique.Where(d => d.Long <= 0.00d && d.Long >= -179.99d && d.Lat <= 89.99d && d.Lat >= 0.00d).Select(d => d.Uuid)
			};
		}

		public async Task Analyze(string jsonFile, string csvFile, string outputFile) {
			var result = await Analyze(jsonFile, csvFile);
			log.Info($@"Writing analysis result to {outputFile}");
			try {
				await resultWriter.WriteResult(result, outputFile);
				log.Info($@"Successfully wrote result to {outputFile}");
			} catch (DeviceAnalysisResultWriterException ex) {
				log.Error($@"An error occurred while writing output file {outputFile}", ex);
			}
		}
	}
}
