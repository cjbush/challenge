﻿using Common.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SystemInterface.IO;

namespace SouthernCrossEngineering.Challenge.Bush {
	internal sealed class DefaultJsonDeviceParser : IJsonDeviceParser {

		private readonly IFile File;
		private readonly ILog log;

		public DefaultJsonDeviceParser(IFile File, ILog log) {
			this.File = File;
			this.log = log;
		}

		public async Task<IEnumerable<Device>> Parse(string file) {
			DeviceList devices = null;
			try {
				var json = File.ReadAllText(file);
				await Task.Run(() => devices = JsonConvert.DeserializeObject<DeviceList>(json)).ConfigureAwait(false);
			} catch (FileNotFoundException) {
				log.Error($@"JSON file {file} was not found.");
			} catch (System.IO.IOException ex) {
				log.Error($@"Error reading JSON input file {file}", ex);
			} catch (JsonException ex) {
				log.Error($@"JSON file {file} contains invalid JSON. Please verify your input file.", ex);
			}
			return devices?.Devices ?? new List<Device>();
		}
	}
}
