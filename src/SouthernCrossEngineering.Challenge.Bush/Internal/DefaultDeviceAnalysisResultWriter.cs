﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using SystemInterface.IO;

namespace SouthernCrossEngineering.Challenge.Bush {
	internal sealed class DefaultDeviceAnalysisResultWriter : IDeviceAnalysisResultWriter {

		private readonly IFile File;
		public DefaultDeviceAnalysisResultWriter(IFile File) {
			this.File = File;
		}

		public async Task WriteResult(DeviceAnalysisResult result, string outputFile) {
			try {
				string json = null;
				await Task.Run(() => json = JsonConvert.SerializeObject(result, Formatting.Indented)).ConfigureAwait(false);
				File.WriteAllText(outputFile, json);
			} catch (JsonException ex) {
				throw new DeviceAnalysisResultWriterException("Error serializing analysis result", ex);
			} catch (System.IO.IOException ex) {
				throw new DeviceAnalysisResultWriterException("Error saving serialized result to file", ex);
			}
		}
	}
}
