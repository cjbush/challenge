﻿using System.Threading.Tasks;

namespace SouthernCrossEngineering.Challenge.Bush {
	internal interface IDeviceAnalysisResultWriter {

		Task WriteResult(DeviceAnalysisResult result, string outputFile);

	}
}
