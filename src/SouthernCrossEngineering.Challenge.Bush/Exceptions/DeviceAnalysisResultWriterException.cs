﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace SouthernCrossEngineering.Challenge.Bush {
	[Serializable]
	[ExcludeFromCodeCoverage]
	internal class DeviceAnalysisResultWriterException : Exception {
		public DeviceAnalysisResultWriterException() {
		}

		public DeviceAnalysisResultWriterException(string message) : base(message) {
		}

		public DeviceAnalysisResultWriterException(string message, Exception innerException) : base(message, innerException) {
		}

		protected DeviceAnalysisResultWriterException(SerializationInfo info, StreamingContext context) : base(info, context) {
		}
	}
}