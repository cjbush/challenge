﻿using Common.Logging;
using Ninject.Modules;
using SystemInterface.IO;
using SystemWrapper.IO;

namespace SouthernCrossEngineering.Challenge.Bush.IoCConfiguration {
	public sealed class DeviceAnalysisModule : NinjectModule {
		public override void Load() {
			//Bind our interfaces to our implementations
			Bind<IDeviceAnalyzer>().To<DefaultDeviceAnalyzer>();
			Bind<ICsvDeviceParser>().To<DefaultCsvDeviceParser>();
			Bind<IJsonDeviceParser>().To<DefaultJsonDeviceParser>();
			Bind<IDeviceAnalysisResultWriter>().To<DefaultDeviceAnalysisResultWriter>();

			//Bind the third party stuff
			Bind<IFile>().To<FileWrap>();
			Bind<ILog>().ToMethod((ctx) => {
				return LogManager.GetLogger(ctx.Request.ParentContext.Plan.Type);
			});
		}
	}
}
