﻿using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.IO;
using System.Threading.Tasks;
using SystemInterface.IO;

namespace SouthernCrossEngineering.Challenge.Bush.Test {
	internal sealed class IDeviceAnalysisResultWriterTest : BaselineTestFixture<IDeviceAnalysisResultWriter, DefaultDeviceAnalysisResultWriter> {

		private DeviceAnalysisResult result;
		private string json;

		protected override void SetUp() {
			result = new DeviceAnalysisResult {
				DevicesInRange = new[] { new Guid("dddb7d25-aefa-422a-b399-9134aa25d73f") },
				TotalNumberOfDevices = 2,
				TotalNumberOfUniqueDevices = 2,
				NewestUpdatedTimestamp = 1490285066,
				OldestUpdatedTimestamp = 1490285065,
				NewestUpdatedIds = new[] { new Guid("acca463e-bc04-4ffe-81e3-3866454fc235"), new Guid("b7bbf5ca-488a-4b37-b900-36a43e8bc104") },
				OldestUpdatedIds = new[] { new Guid("bf74d39f-60fc-4dcc-81cf-3f364d7b5ed7") }
			};
			json = System.IO.File.ReadAllText("result.json");
		}

		[TestCase(TestName = @"IDeviceAnalysisResultWriter should be able to write a JSON serialized analysis result object to a file")]
		public async Task WriteResult_ShouldSerializeAndWriteResult() {
			ninject.GetMock<IFile>().Setup(File => File.WriteAllText("result.json", json)).Verifiable();
			await subject.WriteResult(result, "result.json");
			ninject.MockRepository.Verify();
		}

		[TestCase(TestName = @"IDeviceAnalysisResultWriter should be able to handle a file IO error")]
		public void WriteResult_ShouldHandleIOException() {
			ninject.GetMock<IFile>().Setup(File => File.WriteAllText("result.json", json)).Throws<IOException>();
			Assert.Throws<DeviceAnalysisResultWriterException>(async () => await subject.WriteResult(result, "result.json"));
		}

		[TestCase(TestName = @"IDeviceAnalysisResultWriter should handle a serialization exception")]
		public void WriteResult_ShouldHandleSerializationException() {
			ninject.GetMock<IFile>().Setup(File => File.WriteAllText(It.IsAny<string>(), It.IsAny<string>())).Throws<JsonException>(); // This wouldn't actually happen, but it's easier than trying to break the json parser itself. All we're doing is making sure we handle a JsonException properly
			Assert.Throws<DeviceAnalysisResultWriterException>(async () => await subject.WriteResult(result, "result.json"));
		}

	}
}
