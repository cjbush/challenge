﻿using Ninject;
using Ninject.MockingKernel.Moq;
using NUnit.Framework;
using System;

namespace SouthernCrossEngineering.Challenge.Bush.Test {
	[TestFixture(Category = @"Coding Challenge (Chris Bush)")]
	public abstract class BaselineTestFixture<TInterface, TImplementation> where TImplementation : TInterface {

		//I hate boilerplate, so this handles some of the test fixture setup/teardown for me.

		protected MoqMockingKernel ninject;
		protected TInterface subject;
		protected Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject po; //just in case we need to manually check a private field or something like that

		protected virtual void SetUp() { }
		protected virtual void TearDown() { }
		protected virtual void TestFixtureSetUp() { }
		protected virtual void TestFixtureTearDown() { }


		[SetUp]
		public void SetUp4Realz() {
			ninject = new MoqMockingKernel();
			subject = ninject.Get<TImplementation>(); // This gives us our test subject, with mocks injected into it that we can setup in our test cases
			po = new Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject(subject);
			SetUp();
		}

		[TearDown]
		public void TearDown4Realz() {
			ninject.Dispose(); // Dispose of our IoC container
			(subject as IDisposable)?.Dispose(); // Dispose of the test subject if it's disposable
			TearDown();
		}

		[TestFixtureSetUp]
		public void TestFixtureSetUp4Realz() {
			TestFixtureSetUp();
		}

		[TestFixtureTearDown]
		public void TestFixtureTearDown4Realz() {
			TestFixtureTearDown();
		}

	}
}
