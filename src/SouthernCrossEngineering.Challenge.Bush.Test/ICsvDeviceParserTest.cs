﻿using Common.Logging;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SystemInterface.IO;
using SystemWrapper.IO;

namespace SouthernCrossEngineering.Challenge.Bush.Test {
	internal sealed class ICsvDeviceParserTest : BaselineTestFixture<ICsvDeviceParser, DefaultCsvDeviceParser> {

		[TestCase(TestName = @"ICsvDeviceParser should be able to parse a CSV file and give back a list of devices")]
		public async Task Parse_ShouldParse() {
			ninject.GetMock<IFile>().Setup(File => File.OpenRead("data.csv")).Returns(new FileWrap().OpenRead("data.csv"));

			var devices = await subject.Parse("data.csv");

			Assert.IsNotNull(devices);
			Assert.AreEqual(50, devices.Count());
			Assert.AreEqual(Guid.Parse("744f59d4-36b5-40c7-a2cc-5a6c6cc788a3"), devices.First().Uuid);
			Assert.AreEqual(39.87, devices.First().Lat);
			Assert.AreEqual(-77.275, devices.First().Long);
			Assert.AreEqual(1490285067, devices.First().Timestamp);
		}

		[TestCase(TestName = @"ICsvDeviceParser should handle a non-existent file")]
		public async Task Parse_ShouldHandleMissingFile() {
			var filename = "notfound.csv";
			ninject.GetMock<IFile>().Setup(File => File.OpenRead(filename)).Throws<FileNotFoundException>();
			ninject.GetMock<ILog>().Setup(log => log.Error($@"CSV file {filename} was not found.")).Verifiable();

			var devices = await subject.Parse("notfound.csv");

			Assert.IsNotNull(devices);
			CollectionAssert.IsEmpty(devices);

			ninject.MockRepository.Verify();
		}

		[TestCase(TestName = @"ICsvDeviceParser should handle malformed CSV data")]
		public async Task Parse_ShouldHandleBaddata() {
			var filename = "bad_input.csv";
			ninject.GetMock<IFile>().Setup(File => File.OpenRead(filename)).Returns(new FileWrap().OpenRead(filename));
			ninject.GetMock<ILog>().Setup(log => log.Error("Line 2 was incorrectly formatted. Skipping to the next line.")).Verifiable();
			ninject.GetMock<ILog>().Setup(log => log.Error("Line 4 was incorrectly formatted. Skipping to the next line.")).Verifiable();
			ninject.GetMock<ILog>().Setup(log => log.Error("Line 5 was incorrectly formatted. Skipping to the next line.")).Verifiable();
			ninject.GetMock<ILog>().Setup(log => log.Error("Line 6 was incorrectly formatted. Skipping to the next line.")).Verifiable();
			ninject.GetMock<ILog>().Setup(log => log.Error("Line 7 was incorrectly formatted. Skipping to the next line.")).Verifiable();

			var devices = await subject.Parse("bad_input.csv");

			Assert.IsNotNull(devices);

			Assert.AreEqual(1, devices.Count());
			Assert.AreEqual(Guid.Parse("418ad35a-b929-42fa-abd7-039af222a46f"), devices.First().Uuid);
			Assert.AreEqual(-77.55095, devices.First().Lat);
			Assert.AreEqual(-148.39879, devices.First().Long);
			Assert.AreEqual(1490285066, devices.First().Timestamp);

			ninject.MockRepository.Verify();
		}

	}
}
