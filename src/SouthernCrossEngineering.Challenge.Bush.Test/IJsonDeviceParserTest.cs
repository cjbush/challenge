﻿using Common.Logging;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SystemInterface.IO;
using SystemWrapper.IO;

namespace SouthernCrossEngineering.Challenge.Bush.Test {
	internal sealed class IJsonDeviceParserTest : BaselineTestFixture<IJsonDeviceParser, DefaultJsonDeviceParser> {

		[TestCase(TestName = @"IJsonDeviceParser should be able to parse a JSON file and give back a list of devices")]
		public async Task Parse_ShouldParse() {
			ninject.GetMock<IFile>().Setup(File => File.ReadAllText("data.json")).Returns(new FileWrap().ReadAllText("data.json"));

			var devices = await subject.Parse("data.json");

			Assert.IsNotNull(devices);
			Assert.AreEqual(50, devices.Count());
			Assert.AreEqual(Guid.Parse("5d7c9553-3d2a-467b-acfd-c60bcabd8273"), devices.First().Uuid);
			Assert.AreEqual(23.43115, devices.First().Lat);
			Assert.AreEqual(-54.29007, devices.First().Long);
			Assert.AreEqual(1490285067, devices.First().Timestamp);
		}

		[TestCase(TestName = @"IJsonDeviceParser should handle a missing input file")]
		public async Task Parse_ShouldHandleMissingFile() {
			ninject.GetMock<IFile>().Setup(File => File.ReadAllText("data.json")).Throws<FileNotFoundException>();
			ninject.GetMock<ILog>().Setup(log => log.Error(@"JSON file data.json was not found.")).Verifiable();

			await subject.Parse("data.json");

			ninject.MockRepository.Verify();
		}

		[TestCase(TestName = @"IJsonDeviceParser should handle a file read error")]
		public async Task Parse_ShouldHandleIOException() {
			ninject.GetMock<IFile>().Setup(File => File.ReadAllText("data.json")).Throws<IOException>();
			ninject.GetMock<ILog>().Setup(log => log.Error($@"Error reading JSON input file data.json", It.IsAny<IOException>())).Verifiable();

			await subject.Parse("data.json");

			ninject.MockRepository.Verify();
		}

		[TestCase(TestName = @"IJsonDeviceParser should handle invalid JSON input")]
		public async Task Parse_ShouldHandleInvalidJsonInput() {
			ninject.GetMock<IFile>().Setup(File => File.ReadAllText("data.json")).Returns("thisisinvalidjson");
			ninject.GetMock<ILog>().Setup(log => log.Error($@"JSON file data.json contains invalid JSON. Please verify your input file.", It.IsAny<JsonException>())).Verifiable();

			await subject.Parse("data.json");

			ninject.MockRepository.Verify();
		}

	}
}
