﻿using Common.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SouthernCrossEngineering.Challenge.Bush.Test {
	internal sealed class IDeviceAnalyzerTest : BaselineTestFixture<IDeviceAnalyzer, DefaultDeviceAnalyzer> {

		private IEnumerable<Device> jsonDevices, csvDevices;
		private Guid id1, id2, id3, id4;

		protected override void SetUp() {
			id1 = Guid.NewGuid();
			id2 = Guid.NewGuid();
			id3 = Guid.NewGuid();
			id4 = Guid.NewGuid();

			jsonDevices = new List<Device> {
				new Device {
					Uuid = id1,
					Lat = 40d,
					Long = -150d,
					Timestamp = 1490285066
				},
				new Device {
					Uuid = id2,
					Lat = 95d,
					Long = -140d,
					Timestamp = 1490285028
				}
			};

			csvDevices = new List<Device> {
				new Device {
					Uuid = id3,
					Lat = 80d,
					Long = -47d,
					Timestamp = 1490285048
				},
				new Device {
					Uuid = id4,
					Lat = -120d,
					Long = 46d,
					Timestamp = 1490285030
				},
				new Device {
					Uuid = id1,
					Lat = 40d,
					Long = -150d,
					Timestamp = 1490285066
				},
				new Device {
					Uuid = id1,
					Lat = 40d,
					Long = -150d,
					Timestamp = 1490285066
				},
			};
		}

		[TestCase(TestName = @"IDeviceAnalyzer should be able to analyze two lists of devices")]
		public async Task Analyze_ShouldReturnAValidResult() {
			ninject.GetMock<IJsonDeviceParser>().Setup(jp => jp.Parse("data.json")).Returns(Task.FromResult(jsonDevices)).Verifiable();
			ninject.GetMock<ICsvDeviceParser>().Setup(csvp => csvp.Parse("data.csv")).Returns(Task.FromResult(csvDevices)).Verifiable();

			var result = await subject.Analyze("data.json", "data.csv");

			Assert.IsNotNull(result);
			CollectionAssert.AreEqual(new[] { id1 }, result.NewestUpdatedIds);
			Assert.AreEqual(1490285066, result.NewestUpdatedTimestamp);
			CollectionAssert.AreEqual(new[] { id2 }, result.OldestUpdatedIds);
			Assert.AreEqual(1490285028, result.OldestUpdatedTimestamp);
			Assert.AreEqual(6, result.TotalNumberOfDevices);
			Assert.AreEqual(4, result.TotalNumberOfUniqueDevices);
			CollectionAssert.AreEquivalent(new[] { id1, id3 }, result.DevicesInRange);

			ninject.MockRepository.Verify();
		}

		[TestCase(TestName = @"IDeviceAnalyzer should be able to handle null inputs")]
		public async Task Analyze_ShouldHandleBadInput() {
			ninject.GetMock<IJsonDeviceParser>().Setup(jp => jp.Parse(null)).Returns(Task.FromResult((IEnumerable<Device>)new List<Device> { })).Verifiable();
			ninject.GetMock<ICsvDeviceParser>().Setup(csvp => csvp.Parse(null)).Returns(Task.FromResult((IEnumerable<Device>)new List<Device> { })).Verifiable();
			var result = await subject.Analyze(null, null);

			Assert.IsNotNull(result);
			CollectionAssert.IsEmpty(result.NewestUpdatedIds);
			CollectionAssert.IsEmpty(result.OldestUpdatedIds);
			Assert.AreEqual(default(long), result.NewestUpdatedTimestamp);
			Assert.AreEqual(default(long), result.OldestUpdatedTimestamp);
			Assert.AreEqual(0, result.NumberOfDevicesInRange);
			Assert.AreEqual(0, result.TotalNumberOfDevices);
			Assert.AreEqual(0, result.TotalNumberOfUniqueDevices);
			CollectionAssert.IsEmpty(result.DevicesInRange);
		}

		[TestCase(TestName = @"IDeviceAnalyzer should be able to handle an edge case where the newest device is two devices updated at the same time")]
		public async Task Analyze_HandleSameTimestamp() {
			jsonDevices.First().Timestamp = 1490285028; // set device 1 to have the same timestamp as device 2 and see what happens

			ninject.GetMock<IJsonDeviceParser>().Setup(jp => jp.Parse("data.json")).Returns(Task.FromResult(jsonDevices)).Verifiable();
			ninject.GetMock<ICsvDeviceParser>().Setup(csvp => csvp.Parse("data.csv")).Returns(Task.FromResult(csvDevices)).Verifiable();

			var result = await subject.Analyze("data.json", "data.csv");

			Assert.IsNotNull(result);
			CollectionAssert.AreEquivalent(new[] { id1, id2 }, result.OldestUpdatedIds);
			CollectionAssert.AreEquivalent(new[] { id3 }, result.NewestUpdatedIds);
			Assert.AreEqual(jsonDevices.First().Timestamp, result.OldestUpdatedTimestamp);
			Assert.AreEqual(csvDevices.First().Timestamp, result.NewestUpdatedTimestamp);
			Assert.AreEqual(6, result.TotalNumberOfDevices);
			Assert.AreEqual(4, result.TotalNumberOfUniqueDevices);
			CollectionAssert.AreEquivalent(new[] { id1, id3 }, result.DevicesInRange);
		}

		[TestCase(TestName = @"IDeviceAnalyzer should be able to write an output file for a result")]
		public async Task Analyze_WriteOutputFile() {
			ninject.GetMock<IJsonDeviceParser>().Setup(jp => jp.Parse("data.json")).Returns(Task.FromResult((IEnumerable<Device>)new List<Device> { })).Verifiable();
			ninject.GetMock<ICsvDeviceParser>().Setup(csvp => csvp.Parse("data.csv")).Returns(Task.FromResult((IEnumerable<Device>)new List<Device> { })).Verifiable();
			ninject.GetMock<IDeviceAnalysisResultWriter>().Setup(w => w.WriteResult(It.IsAny<DeviceAnalysisResult>(), "result.json")).Returns(Task.FromResult(0)).Verifiable();
			await subject.Analyze("data.json", "data.csv", "result.json");
			ninject.MockRepository.Verify();
		}

		[TestCase(TestName = @"IDeviceAnalyzer should handle an error when writing the result file")]
		public async Task Analyze_HandleFileError() {
			ninject.GetMock<IJsonDeviceParser>().Setup(jp => jp.Parse("data.json")).Returns(Task.FromResult((IEnumerable<Device>)new List<Device> { })).Verifiable();
			ninject.GetMock<ICsvDeviceParser>().Setup(csvp => csvp.Parse("data.csv")).Returns(Task.FromResult((IEnumerable<Device>)new List<Device> { })).Verifiable();
			ninject.GetMock<IDeviceAnalysisResultWriter>().Setup(w => w.WriteResult(It.IsAny<DeviceAnalysisResult>(), "result.json")).Throws<DeviceAnalysisResultWriterException>();
			ninject.GetMock<ILog>().Setup(log => log.Error("An error occurred while writing output file result.json", It.IsAny<DeviceAnalysisResultWriterException>())).Verifiable();
			await subject.Analyze("data.json", "data.csv", "result.json");
			ninject.MockRepository.Verify();
		}

	}
}
